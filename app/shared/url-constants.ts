export class UrlConstants {
    
    public static BASE_URL: string = "http://localhost:3002";

    public static LANGUAGES: string = UrlConstants.BASE_URL + "/languages";
    public static LANGUAGE(id: number): string { return UrlConstants.LANGUAGES + "/" + id; }

    private static COMPONENTS: string = UrlConstants.BASE_URL + "/components";
    public static COMPONENT(id: number): string { return UrlConstants.COMPONENTS + "/" + id; }
    public static COMPONENTS_FOR_LANGUAGE(languageId: number): string { return UrlConstants.COMPONENTS + "?language=" + languageId; }

    private static FILES: string = UrlConstants.BASE_URL + "/files";
    public static FILES_FOR_COMPONENT(componentId: number): string { return UrlConstants.FILES + "?component=" + componentId; }

}