"use strict";
var UrlConstants = (function () {
    function UrlConstants() {
    }
    UrlConstants.LANGUAGE = function (id) { return UrlConstants.LANGUAGES + "/" + id; };
    UrlConstants.COMPONENT = function (id) { return UrlConstants.COMPONENTS + "/" + id; };
    UrlConstants.COMPONENTS_FOR_LANGUAGE = function (languageId) { return UrlConstants.COMPONENTS + "?language=" + languageId; };
    UrlConstants.FILES_FOR_COMPONENT = function (componentId) { return UrlConstants.FILES + "?component=" + componentId; };
    UrlConstants.BASE_URL = "http://localhost:3002";
    UrlConstants.LANGUAGES = UrlConstants.BASE_URL + "/languages";
    UrlConstants.COMPONENTS = UrlConstants.BASE_URL + "/components";
    UrlConstants.FILES = UrlConstants.BASE_URL + "/files";
    return UrlConstants;
}());
exports.UrlConstants = UrlConstants;
