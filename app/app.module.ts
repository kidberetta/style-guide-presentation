import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { AppComponent }   from './app.component';
import { routing, appRoutingProviders } from './app.routing';
import { HttpModule, JsonpModule } from '@angular/http';
import { LanguageComponent } from './language/language.component';
import { ComponentComponent } from './component/component.component';
import { LanguageService } from './language/language.service';
import { ComponentService } from './component/component.service';
import { FileService } from './file/file.service';
import { AppInfoComponent } from './app-info/app-info.component';

@NgModule({
    imports: [BrowserModule, routing, FormsModule, HttpModule, JsonpModule],
    declarations: [AppComponent, LanguageComponent, AppInfoComponent, ComponentComponent],
    providers: [appRoutingProviders, LanguageService, ComponentService, FileService],
    bootstrap: [AppComponent]
})
export class AppModule {

}
