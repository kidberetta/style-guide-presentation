"use strict";
var router_1 = require('@angular/router');
var app_info_component_1 = require('./app-info/app-info.component');
var language_component_1 = require('./language/language.component');
var component_component_1 = require('./component/component.component');
var appRoutes = [
    { path: '', component: app_info_component_1.AppInfoComponent },
    { path: 'languages/:id', component: language_component_1.LanguageComponent },
    { path: 'components/:id', component: component_component_1.ComponentComponent }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
