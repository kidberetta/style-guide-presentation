import { Component, OnInit } from '@angular/core';
import { LanguageService } from './language/language.service';
import { Language } from './language/language';
import { Router } from '@angular/router';

@Component({
  selector: 'my-app',
  templateUrl: './app/app.component.html'
})
export class AppComponent implements OnInit {

  private languageService: LanguageService;
  private router:Router;
  private languages: Language[];

  constructor(languageService: LanguageService,router:Router) {
    this.languageService = languageService;
    this.router = router;
  }

  ngOnInit() {
    this.languageService.getLanuages().subscribe(
      data => this.languages = data.json(),
      error => console.log("could not read error :" + error)
    );
  }

  goToLanguage(language:Language)  {
    this.router.navigate(['/languages', language.id]);
  }

}
