"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var language_service_1 = require('./language.service');
var component_service_1 = require('../component/component.service');
var language_1 = require('./language');
var LanguageComponent = (function () {
    function LanguageComponent(activatedRoute, router, languageService, componentService) {
        this.language = new language_1.Language({});
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.languageService = languageService;
        this.componentService = componentService;
    }
    LanguageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.forEach(function (params) {
            var id = params['id'];
            _this.languageService.getLanuage(id).subscribe(function (data) { return _this.setComponents(data); });
        });
    };
    LanguageComponent.prototype.setComponents = function (data) {
        var _this = this;
        this.language = new language_1.Language(data.json());
        this.componentService.getComponentsForLanguage(this.language.id).subscribe(function (data) { return _this.components = data.json(); });
    };
    LanguageComponent.prototype.goToComponent = function (component) {
        console.log("I'm here first - " + component.id + "-");
        this.router.navigate(['/components', component.id]);
    };
    LanguageComponent = __decorate([
        core_1.Component({
            selector: 'language',
            templateUrl: './app/language/language.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, language_service_1.LanguageService, component_service_1.ComponentService])
    ], LanguageComponent);
    return LanguageComponent;
}());
exports.LanguageComponent = LanguageComponent;
