"use strict";
var Language = (function () {
    function Language(iLanguage) {
        this._id = iLanguage.id;
        this._name = iLanguage.name;
    }
    Object.defineProperty(Language.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (languageId) {
            this._id = languageId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Language.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    return Language;
}());
exports.Language = Language;
