export interface ILanguage  {
    id?:number,
    name?:string
}

export class Language{

    private _id:number;
    private _name:string;

    constructor(iLanguage:ILanguage)   {
        this._id = iLanguage.id;
        this._name = iLanguage.name;
    }

    public get id():number  {
        return this._id;
    }
    public set id(languageId:number)    {
        this._id = languageId;
    }
    public get name():string  {
        return this._name;
    }
    public set name(name:string)    {
        this._name = name;
    }

}