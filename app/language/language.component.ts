import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { LanguageService } from './language.service';
import { ComponentService } from '../component/component.service';
import { Language } from './language';
import { ComponentDto } from '../component/component';

@Component({
  selector: 'language',
  templateUrl: './app/language/language.component.html'
})
export class LanguageComponent implements OnInit {

  private activatedRoute: ActivatedRoute;
  private router:Router;
  private languageService: LanguageService;
  private componentService: ComponentService;
  private language: Language = new Language({});
  private components: ComponentDto[];

  constructor(activatedRoute: ActivatedRoute,router:Router, languageService: LanguageService, componentService: ComponentService) {
    this.router = router;
    this.activatedRoute = activatedRoute;
    this.languageService = languageService;
    this.componentService = componentService;
  }

  ngOnInit() {
    this.activatedRoute.params.forEach((params: Params) => {
      let id: number = params['id'];
      this.languageService.getLanuage(id).subscribe(data => this.setComponents(data));
    });
  }

  setComponents(data: any) {
    this.language = new Language(data.json());
    this.componentService.getComponentsForLanguage(this.language.id).subscribe(data => this.components = data.json())
  }

  goToComponent(component: ComponentDto) {
    console.log("I'm here first - " + component.id + "-");
    this.router.navigate(['/components', component.id]);
  }

}
