import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { UrlConstants } from'../shared/url-constants';

@Injectable()
export class LanguageService {

    http: Http;

    constructor(http: Http) {
        this.http = http;
    }

    public getLanuages(): Observable<Response> {
        return this.http.get(UrlConstants.LANGUAGES);
    }

    public getLanuage(id:number): Observable<Response> {
        return this.http.get(UrlConstants.LANGUAGE(id));
    }

}