import { IComponent, ComponentDto } from '../component/component';

export interface IFile {
    id?: number,
    component?: IComponent,
    name?: string,
    path?: string,
    content?: string
}

export class File {

    private _id: number;
    private _component: ComponentDto;
    private _name: string;
    private _path: string;
    private _content: string;

    constructor(iFile: IFile) {
        this._id = iFile.id;
        //this._component = new ComponentDto(iFile.component);
        this._name = iFile.name;
        this._path = iFile.path;
        this._content = iFile.content;
    }

    public get id(): number {
        return this._id;
    }
    public set id(val: number) {
        this._id = val;
    }
    public get component(): ComponentDto {
        return this._component;
    }
    public set component(val: ComponentDto) {
        this._component = val;
    }
    public get name(): string {
        return this._name;
    }
    public set name(val: string) {
        this._name = val;
    }
    public get path(): string {
        return this._path;
    }
    public set path(val: string) {
        this._path = val;
    }
    public get content(): string {
        return this._content;
    }
    public set content(val: string) {
        this._content = val;
    }

}