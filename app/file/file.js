"use strict";
var File = (function () {
    function File(iFile) {
        this._id = iFile.id;
        //this._component = new ComponentDto(iFile.component);
        this._name = iFile.name;
        this._path = iFile.path;
        this._content = iFile.content;
    }
    Object.defineProperty(File.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (val) {
            this._id = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(File.prototype, "component", {
        get: function () {
            return this._component;
        },
        set: function (val) {
            this._component = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(File.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (val) {
            this._name = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(File.prototype, "path", {
        get: function () {
            return this._path;
        },
        set: function (val) {
            this._path = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(File.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (val) {
            this._content = val;
        },
        enumerable: true,
        configurable: true
    });
    return File;
}());
exports.File = File;
