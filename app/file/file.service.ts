import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { UrlConstants } from'../shared/url-constants';

@Injectable()
export class FileService {

    http: Http;

    constructor(http: Http) {
        this.http = http;
    }

    public getFilesForComponent(componentId:number): Observable<Response> {
        return this.http.get(UrlConstants.FILES_FOR_COMPONENT(componentId));
    }

}