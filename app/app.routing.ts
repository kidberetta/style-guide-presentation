import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';
import { AppComponent } from './app.component';
import { AppInfoComponent } from './app-info/app-info.component';
import { LanguageComponent } from './language/language.component';
import { ComponentComponent } from './component/component.component';

const appRoutes: Routes = [
    { path: '', component: AppInfoComponent },
    { path: 'languages/:id', component: LanguageComponent },
    { path: 'components/:id', component: ComponentComponent }
];

export const appRoutingProviders: any[] = [
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
