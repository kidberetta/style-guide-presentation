import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { UrlConstants } from'../shared/url-constants';

@Injectable()
export class ComponentService {

    http: Http;

    constructor(http: Http) {
        this.http = http;
    }

    public getComponent(id:number): Observable<Response> {
        return this.http.get(UrlConstants.COMPONENT(id));
    }

    public getComponentsForLanguage(id:number): Observable<Response> {
        return this.http.get(UrlConstants.COMPONENTS_FOR_LANGUAGE(id));
    }

    

}