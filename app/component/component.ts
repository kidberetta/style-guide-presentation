import { ILanguage, Language } from '../language/language';

export interface IComponent {
    id?: number,
    name?: string,
    discription?:string,
    language?: ILanguage
}

export class ComponentDto { //TODO learn how to use namespaces so you can change this to Component
    _id: number;
    _name: string;
    _language: Language;
    _discription:string;

    constructor(iComponent:IComponent)   {
        this._id = iComponent.id;
        this._name = iComponent.name;
        this._discription = iComponent.discription;
        //this._language = new Language(iComponent.language);
    }

    public get id(): number {
        return this._id;
    }
    public set id(val: number) {
        this._id = val;
    }
    public get name(): string {
        return this._name;
    }
    public set name(val: string) {
        this._name = val;
    }
    public get discription(): string {
        return this._discription;
    }
    public set discription(val: string) {
        this._discription = val;
    }
    public get language(): Language {
        return this._language;
    }
    public set language(val: Language) {
        this._language = val;
    }

}