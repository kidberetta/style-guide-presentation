import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ComponentService } from './component.service';
import { FileService } from '../file/file.service';
import { ComponentDto } from './component';
import { File } from '../file/file';

@Component({
    selector: 'component',
    templateUrl: './app/component/component.component.html'
})
export class ComponentComponent implements OnInit {

    private activatedRoute: ActivatedRoute;
    private router: Router;
    private componentService: ComponentService;
    private fileService: FileService;
    private component: ComponentDto = new ComponentDto({});
    private files: File[];
    private activeFile:File;

    constructor(activatedRoute: ActivatedRoute, router: Router, fileService: FileService, componentService: ComponentService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.componentService = componentService;
        this.fileService = fileService;
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id: number = params['id'];
            this.componentService.getComponent(id).subscribe(data => this.setComponent(data));
        });
    }

    setComponent(data: any) {
        this.component = data.json();
        this.fileService.getFilesForComponent(this.component.id).subscribe(data=>this.files = data.json());
    }

    viewFileDetails(file:File)   {
        this.activeFile = file;
    }

}