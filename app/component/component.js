"use strict";
var ComponentDto = (function () {
    function ComponentDto(iComponent) {
        this._id = iComponent.id;
        this._name = iComponent.name;
        this._discription = iComponent.discription;
        //this._language = new Language(iComponent.language);
    }
    Object.defineProperty(ComponentDto.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (val) {
            this._id = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComponentDto.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (val) {
            this._name = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComponentDto.prototype, "discription", {
        get: function () {
            return this._discription;
        },
        set: function (val) {
            this._discription = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ComponentDto.prototype, "language", {
        get: function () {
            return this._language;
        },
        set: function (val) {
            this._language = val;
        },
        enumerable: true,
        configurable: true
    });
    return ComponentDto;
}());
exports.ComponentDto = ComponentDto;
